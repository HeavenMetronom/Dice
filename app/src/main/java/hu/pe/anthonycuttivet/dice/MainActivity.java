package hu.pe.anthonycuttivet.dice;

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder().deleteRealmIfMigrationNeeded().build();
        Realm realm = Realm.getInstance(config);
        realm.beginTransaction();
        RealmResults<User> data = realm.where(User.class)
                .equalTo("id", 0)
                .findAll();
        ArrayList<User> list = new ArrayList(data);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        try {
            this.sendGetRequest();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Spinner sItems = (Spinner) findViewById(R.id.spinner);
        sItems.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                TextView rolled = (TextView) findViewById(R.id.rolled_text);
                rolled.setVisibility(rolled.INVISIBLE);
                ImageView img = (ImageView) findViewById(R.id.imageView2);
                Spinner sItems = (Spinner) findViewById(R.id.spinner);
                String selectedDie = sItems.getSelectedItem().toString();
                String faces = selectedDie.substring(selectedDie.lastIndexOf("(") + 1).substring(0,2);
                faces = faces.replaceAll(" ","");
                switch (faces){
                    case "4" : img.setBackgroundResource(R.drawable.d4);
                    break;
                    case "6" : img.setBackgroundResource(R.drawable.d6);
                        break;
                    case "8" : img.setBackgroundResource(R.drawable.d8);
                        break;
                    case "10" : img.setBackgroundResource(R.drawable.d10);
                        break;
                    case "12" : img.setBackgroundResource(R.drawable.d12);
                        break;
                    case "20" : img.setBackgroundResource(R.drawable.d20);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        TextView user = (TextView) findViewById(R.id.username_label);
        if(list.size() != 0 ){
            String username = list.get(0).getUsername();
            user.setText("Welcome " + username + " !");
            String favDie = list.get(0).getFavorite_die();
            int pos = dicesArray.indexOf(favDie);
            sItems.setSelection(pos);
        }
        else{
            user.setText("Welcome USER !");
        }


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.preferences) {
            Intent intent = new Intent(this, Preferences.class);
            intent.putExtra("DIE_LIST", (Serializable) dicesArray);
            startActivity(intent);
        } else if (id == R.id.nav_share) {
            try {
                sendGetRequest();
                if (dices.length() != 0) {
                    Toast.makeText(getApplicationContext(),
                            "Connection OK",
                            Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    //functions

    JSONObject dices = new JSONObject();
    List<String> dicesArray =  new ArrayList<String>();



    public String getSingleDice(String name) throws JSONException {
        return dices.getString(name);
    }


    public void sendGetRequest() throws JSONException {
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(this);
        String url ="https://api.myjson.com/bins/m9irf";
        final String[] jsonResponse = {};


        // Request a string response from the provided URL.
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("MYACTIVITY", response.toString());

                try {
                    // Parsing json object response
                    // response will be a json object
                    dices = response.getJSONObject("dices");
                    if(dices.length() != 0){
                        for(Iterator<String> iter = dices.keys(); iter.hasNext();) {
                            String key = iter.next();
                            dicesArray.add(key + " (" + getSingleDice(key) + " faces)");
                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                                    MainActivity.this, android.R.layout.simple_spinner_item, dicesArray);

                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            Spinner sItems = (Spinner) findViewById(R.id.spinner);
                            sItems.setAdapter(adapter);

                            RealmConfiguration config = new RealmConfiguration.Builder().deleteRealmIfMigrationNeeded().build();
                            Realm realm = Realm.getInstance(config);
                            RealmResults<User> data = realm.where(User.class)
                                    .equalTo("id", 0)
                                    .findAll();
                            ArrayList<User> list = new ArrayList(data);

                            if(list.size() != 0 ){
                                String favDie = list.get(0).getFavorite_die();
                                int pos = dicesArray.indexOf(favDie);
                                sItems.setSelection(pos);
                            }

                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("MYACTIVITY", "Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_SHORT).show();
                // hide the progress dialog
            }
        });
        // Add the request to the RequestQueue.
        queue.add(jsonObjReq);
    }

    public void rollSelectedDie(View view){
        Spinner sItems = (Spinner) findViewById(R.id.spinner);
        String selectedDie = sItems.getSelectedItem().toString();
        String faces = selectedDie.substring(selectedDie.lastIndexOf("(") + 1).substring(0,2);
        faces = faces.replaceAll(" ","");
        int max = Integer.parseInt(faces);
        Random rand = new Random();
        int roll = rand.nextInt(max - 1 + 1) + 1;
        TextView rolled = (TextView) findViewById(R.id.rolled_text);
        rolled.setText("You rolled a " + roll + " !");
        if(rolled.getVisibility() == rolled.INVISIBLE){
            rolled.setVisibility(rolled.VISIBLE);
        }
    }

    public Context getContext() {
        return this;
    }
}
