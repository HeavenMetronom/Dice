package hu.pe.anthonycuttivet.dice;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

public class Preferences extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder().deleteRealmIfMigrationNeeded().build();
        Realm realm = Realm.getInstance(config);
        RealmResults<User> data = realm.where(User.class)
                .equalTo("id", 0)
                .findAll();
        ArrayList<User> list = new ArrayList(data);


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preferences);
        List<String> dicesArray = (List<String>) getIntent().getSerializableExtra("DIE_LIST");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                Preferences.this, android.R.layout.simple_spinner_item, dicesArray);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Spinner sItems = (Spinner) findViewById(R.id.spinner2);
        sItems.setAdapter(adapter);

        if(list.size() != 0 ){
            String username = list.get(0).getUsername();
            String favDie = list.get(0).getFavorite_die();
            EditText userText = findViewById(R.id.editText_username);
            userText.setText(username);
            int pos = dicesArray.indexOf(favDie);
            sItems.setSelection(pos);
        }


    }

    public void saveUserPreferences(View view){
        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder().deleteRealmIfMigrationNeeded().build();
        Realm realm = Realm.getInstance(config);
        Spinner sItems = (Spinner) findViewById(R.id.spinner2);
        // All writes are wrapped in a transaction
        // to facilitate safe multi threading
        EditText userText = findViewById(R.id.editText_username);

        //realm.beginTransaction();
        RealmResults<User> toDel = realm.where(User.class)
                        /*.greaterThan("age", 10)  // implicit AND
                        .beginGroup()*/
                .equalTo("id", 0)
                        /*.or()
                        .contains("name", "Jo")
                        .endGroup()*/
                .findAll();
        toDel.deleteAllFromRealm();


        String selectedDie = sItems.getItemAtPosition(sItems.getSelectedItemPosition()).toString();

        // Add a user
        User user = realm.createObject(User.class);
        user.setUsername(userText.getText().toString());
        user.setFavorite_die(selectedDie);
        realm.commitTransaction();

        RealmResults<User> result = realm.where(User.class)
                        /*.greaterThan("age", 10)  // implicit AND
                        .beginGroup()*/
                .equalTo("id", 0)
                        /*.or()
                        .contains("name", "Jo")
                        .endGroup()*/
                .findAll();


        Toast.makeText(getApplicationContext(),
                "Preferences successfully saved!",
                Toast.LENGTH_LONG).show();

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
