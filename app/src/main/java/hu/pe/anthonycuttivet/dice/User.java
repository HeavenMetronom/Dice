package hu.pe.anthonycuttivet.dice;

import io.realm.RealmModel;
import io.realm.RealmObject;

/**
 * Created by ca500185 on 18/12/2017.
 */

public class User extends RealmObject {
    public String username = "";
    public String favorite_die = "";
    public int id;

    public User(){
        this.username = "";
        this.id = 0;
    }

    public void setUsername(String username){
        this.username = username;
    }

    public String getUsername(){
        return this.username;
    }

    public void setFavorite_die(String favDie){
        this.favorite_die = favDie;
    }

    public String getFavorite_die(){
        return this.favorite_die;
    }

}
